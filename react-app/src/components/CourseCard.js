//import {useState, useEffect} from "react";
import {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import {Card, Button} from "react-bootstrap";
 

export default function CourseCard({courseProp}) {
	// console.log(props)
	// console.log(typeof props)

	// console.log(`CourseName: {props.courseProp.name}`)

	// After changing props to {courseProp}
	// console.log(`CourseName: {courseProp.name}`)

	const {name, description, price, _id} = courseProp;
	console.log(courseProp);
	/*
		SYNTAX:
			const [getter,setter] = useState(initialGetterValue)
	*/
/*	
	const [count,setCount] = useState(0)

	function enroll() {
		// if (count === 30) {
		// 	alert ("No more seats.");
		// } else {
		setCount(count + 1)
		console.log(`Enrollees: ` + count)
		//}
	}
*/

/*	
	const [count,setSeat] = useState(30)

	function seats() {
		if (count !== 0) {
			setSeat(count - 1)
		} else {
			alert ("No more seats.");
		}
	}
*/
	const [count,setCount] = useState(0)
	const [seat,setSeat] = useState(30)

/*	
	function enroll() {
		if (seat !== 0) {
			setSeat(seat - 1)
			console.log(seat)
			setCount(count + 1)
		} else {
			alert ("No more seats.");
		}
	}
*/

	function enroll() {
		setCount(count + 1);
		setSeat(seat - 1);
	}

	useEffect(() => {
		if(seat === 0) {
			alert ("No more seats.");
		}
	}, [seat]);

	return (
		<Card>
			<Card.Body>
			<Card.Title>{name}</Card.Title>		
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>

					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>	
{/*					<Card.Text>Enrollees: {count}</Card.Text>		
			<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
			<Link className = "btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}