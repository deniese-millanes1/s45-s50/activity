/*
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
*/
import {Button, Row, Col} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function Banner({notFound}) {

	//console.log(notFound)
	const { title, content, destination, label} = notFound
	
	return (
		<Row>
{/*	
			{
			notFound ? 
					
						<Col className="p-5">
							<h1>Zuitt Coding Bootcamp</h1>
							<p>Page not found</p>
							<Button variant="info" as = {Link} to="/">Go back to homepage</Button>
						</Col>
						
				:
			
					
						<Col className="p-5">
							<h1>Zuitt Coding Bootcamp</h1>
							<p>Opportunities for everyone, everywhere.</p>
							<Button variant="primary">Enroll Now!</Button>
						</Col>

			}
*/}
		
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
				{/*<Link to={destination}>{label}</Link>*/}
			</Col>
		


		</Row>
	)
} 

