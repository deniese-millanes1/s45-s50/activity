import {Fragment} from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import CourseCard from "../components/CourseCard";

export default function Home() {
		const notFound = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll Now!"

	}

	return (
		<Fragment>
			{/*<Banner />*/}
			<Banner notFound={notFound} />
			<Highlights />
			{/*<CourseCard /> */}
		</Fragment>
	)
}