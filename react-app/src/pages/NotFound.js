import PropTypes from "prop-types";
import Banner from "../components/Banner";


export default function NotFound() {


/*	
	return (
		<Banner notFound ={true}/>
	)
*/

	const notFound = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back Home"
	}

	return(
			<Banner notFound={notFound} />
		)

} 