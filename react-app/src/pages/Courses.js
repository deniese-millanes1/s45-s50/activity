import PropTypes from "prop-types";
import {Fragment, useEffect, useState} from "react";
import CourseCard from "../components/CourseCard";
//import coursesData from "../data/coursesData";


export default function Courses() {
	//console.log(coursesData)

/*START MOCKDATA*/	
/*
	//dataName.map()
	const courses = coursesData.map(course => {
		return ( 
			<CourseCard key={course.id} courseProp={course} />
		)		
	}
	)
*/
/*END MOCKDATA*/	
	
	//State that will be used to store
	const [courses, setCourses] = useState([]);


	useEffect(() => {
		fetch("http://localhost:4000/courses")
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course} />)
			}))
		})
	}, [])

	return (
		<Fragment>
	{/*One 1 course*/}
			{/*<CourseCard courseProp={coursesData[0]} />*/}

	{/*Multiple courses and used map*/}
			{courses} 
		</Fragment>
	)
}


CourseCard.propTypes = {
	courseProp: PropTypes.shape(
		{
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired
		}
	)
}