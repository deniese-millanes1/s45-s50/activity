import PropTypes from "prop-types";
import Banner from "../components/Banner";


export default function NotAllowed() {


	const notAllowed = {
		title: "403 - Forbidden Error",
		content: 'You don’t have permission to access on this server',
		destination: "/",
		label: "Back Home"
	}

	return(
			<Banner notFound={notAllowed} />
		)

} 