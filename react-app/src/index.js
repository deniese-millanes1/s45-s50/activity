import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import AppNavBar from './AppNavBar';
//Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


/*//Render Element
//const name = "John Smith";
const user = {
  firstName: "Jane",
  lastName: "Smith"
}

function formatName(user) {
  return user.firstName + " " + user.lastName
} 

//const element = <h1> Hello, {name} </h1>
const element = <h1> Hello, {formatName(user)} </h1>

//para makita sa browser
ReactDOM.render(
  element,
  document.getElementById("root")
);
*/